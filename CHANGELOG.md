# Money Tree change log

## Version 1.0.0 under development

- New: Added templates for all pages
- New: Added Ubuntu & Inter fonts
- New: Added Swiper
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init June 19, 2023
