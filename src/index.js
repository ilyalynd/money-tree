
// js
import './js/main';

// css
import './assets/css/main.css';

// scss
import '../node_modules/swiper/swiper.scss';
import '../node_modules/swiper/modules/navigation/navigation.scss';
import './assets/scss/main.scss';
