import Swiper, { Navigation } from 'swiper';

(function () {
  const html = document.querySelector('html');

  const mobile = window.matchMedia('(max-width: 1023px)');
  const tablet = window.matchMedia('(min-width: 1024px) and (max-width: 1279px)');
  const desktop = window.matchMedia('(min-width: 1280px)');


  // Плавная прокрутка к якорю
  const anchors = document.querySelectorAll('[data-action=scroll-to]');

  function scrollTo(event) {
    let sectionId = this.getAttribute('href').split('#')[1],
        menu = document.getElementById('menu');

    event.preventDefault();

    if (menu.classList.contains('open')) {
      toggleMenu(event);
    }

    document.getElementById(sectionId).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  }

  anchors.forEach(anchor => anchor.addEventListener('click', scrollTo));


  // Показ хедера при прокрутке вверх / скрытие при прокрутке вниз
  const scrollHeader = () => {
    const header = document.querySelector('header');
    const banner = document.getElementById('banner');
    const roadmapImage =  document.querySelector('.roadmap__image');

    let prevScroll = window.scrollY,
        currentScroll;

    window.addEventListener('scroll', () => {
      currentScroll = window.scrollY;

      if (currentScroll <= 0) {
        header.classList.remove('hidden');
        banner.classList.remove('top');
        return;
      }

      const headerHidden = () => header.classList.contains('hidden');

      if (currentScroll > prevScroll && !headerHidden()) {
        header.classList.add('hidden');
        banner.classList.add('top');
        roadmapImage.style.top = '132px';
      }
      if (currentScroll < prevScroll && headerHidden()) {
        header.classList.remove('hidden');
        banner.classList.remove('top');
        roadmapImage.style.top = '227px';
      }

      prevScroll = currentScroll;
    });
  }

  scrollHeader();


  // Закрытие баннера
  const linkBanner = document.querySelectorAll('[data-action=close-banner]');

  function closeBanner(event) {
    event.preventDefault();

    let banner = document.getElementById('banner'),
        index = document.getElementById('index');

    banner.classList.add('hidden');
    index.classList.add('hidden-banner');
  }

  linkBanner.forEach(link => link.addEventListener('click', closeBanner));


  // Открытие / закрытие меню
  const linkMenu = document.querySelectorAll('[data-action=toggle-menu]');

  function toggleMenu(event) {
    event.preventDefault();

    let menu = document.getElementById('menu'),
        link = document.querySelector('.header-menu-bar a'),
        icon = link.querySelector('svg use');

    // Плавная прокрутка к началу страницы, чтобы выровнять меню
    window.scroll({
      top: 0,
      behavior: 'auto'
    });

    if (menu.classList.contains('open')) {
      link.setAttribute('title', 'Открыть меню');
      icon.setAttribute('href', '/image/icon/menu.svg#menu');

      html.style.overflowY = 'visible';
      menu.classList.remove('open');
    }

    else {
      link.setAttribute('title', 'Закрыть меню');
      icon.setAttribute('href', '/image/icon/close.svg#close');

      menu.classList.add('open');
      html.style.overflowY = 'hidden';
    }

    event.stopPropagation();
  }

  linkMenu.forEach(link => link.addEventListener('click', toggleMenu));


  // Размер дуги
  // const howToBuy = document.querySelector('.how-to-buy')

  // function sizeCircle() {
  //   let howToBuyHeight = howToBuy.offsetHeight / 2,
  //       circle = document.querySelector('.how-to-buy-steps__bg');

  //   circle.style.width = howToBuyHeight;
  //   circle.style.height = howToBuyHeight;
  // }

  // sizeCircle();


  // Слайдер с партнёрами
  let slider,
      init = false;

  function swiperMode() {
    if (mobile.matches) {
      init = false;
      if (slider !== undefined) slider.destroy();
    }

    else if (tablet.matches) {
      init = false;
      if (slider !== undefined) slider.destroy();
    }

    else if (desktop.matches) {
      if (!init) {
        init = true;

        slider = new Swiper('.partners__main', {
          slidesPerView: 6,
          spaceBetween: 20,
          loop: true,

          modules: [ Navigation ],
          navigation: {
            prevEl: '.partners-nav__prev',
            nextEl: '.partners-nav__next'
          }
        });
      }
    }
  }

  const sliders = document.querySelectorAll('.partners__main');

  sliders?.forEach(slider => {
    slider.addEventListener('load', swiperMode());
    slider.addEventListener('resize', swiperMode());
  });


  // Анимации
  if (desktop.matches) {
    const indexLines = document.querySelector('.index__illustration--lines');
    const indexLines2 = document.querySelector('.index__illustration--lines-2');
    const indexShapes = document.querySelector('.index__illustration--shapes');

    const about = document.querySelector('.about');
    const aboutLines = document.querySelector('.about__illustration--lines');

    const aboutClouds = document.querySelector('.about__illustration--cloud');
    const aboutClouds2 = document.querySelector('.about__illustration--cloud-2');
    const aboutClouds3 = document.querySelector('.about__illustration--cloud-3');
    const aboutClouds4 = document.querySelector('.about__illustration--cloud-4');
    const aboutClouds5 = document.querySelector('.about__illustration--cloud-5');
    const aboutClouds6 = document.querySelector('.about__illustration--cloud-6');
    const aboutClouds7 = document.querySelector('.about__illustration--cloud-7');

    const roadmap = document.querySelector('.roadmap');
    const roadmapLines = document.querySelector('.roadmap__illustration--lines');
    const roadmapLines2 = document.querySelector('.roadmap__illustration--lines-2');

    const tokenomics = document.querySelector('.tokenomics');
    const tokenomicsLines = document.querySelector('.tokenomics__illustration--lines');
    const tokenomicsLines2 = document.querySelector('.tokenomics__illustration--lines-2');

    const howToBuy = document.querySelector('.how-to-buy');
    const howToBuyLines = document.querySelector('.how-to-buy__illustration--lines');
    const howToBuyLines2 = document.querySelector('.how-to-buy__illustration--lines-2');

    const partners = document.querySelector('.partners');
    const partnersShapes = document.querySelector('.partners__illustration--shapes');
    const partnersLines = document.querySelector('.partners__illustration--lines');

    const scrollMain = () => {
      window.addEventListener('scroll', () => {
        let aboutTop = about.getBoundingClientRect().top - window.screen.height,
            roadmapTop = roadmap.getBoundingClientRect().top - window.screen.height,
            tokenomicsTop = tokenomics.getBoundingClientRect().top - window.screen.height,
            howToBuyTop = howToBuy.getBoundingClientRect().top - window.screen.height,
            partnersTop = partners.getBoundingClientRect().top - window.screen.height;

        let currentScroll = window.scrollY,
            scrollSpeed = currentScroll / 100;

        indexLines.style.transform = 'translate(-' + (scrollSpeed * 20) + '%, 0)';
        indexLines2.style.transform = 'translate(' + (scrollSpeed * 12) + '%, 0)';
        indexShapes.style.transform = 'rotate(' + (scrollSpeed * 20) + 'deg)';

        aboutClouds.style.transform = 'translate(-' + (scrollSpeed * 8) + '%, 0)';
        aboutClouds2.style.transform = 'translate(-' + (scrollSpeed * 9) + '%, 0)';
        aboutClouds5.style.transform = 'translate(-' + (scrollSpeed * 7) + '%, 0)';
        aboutClouds7.style.transform = 'translate(-' + (scrollSpeed * 6) + '%, 0)';

        aboutClouds3.style.transform = 'translate(' + (scrollSpeed * 7) + '%, 0)';
        aboutClouds4.style.transform = 'translate(' + (scrollSpeed * 8) + '%, 0)';
        aboutClouds6.style.transform = 'translate(' + (scrollSpeed * 8) + '%, 0)';

        if (aboutTop <= 0) {
          aboutLines.style.transform = 'rotate(90deg) translate(-' + (scrollSpeed * 12) + '%, 0)';
        }

        if (roadmapTop <= 0) {
          roadmapLines.style.transform = 'rotate(90deg) translate(-' + (scrollSpeed * 14) + '%, 0)';
          roadmapLines2.style.transform = 'translate(' + (scrollSpeed * 12) + '%, 0)';
        }

        if (tokenomicsTop <= 50) {
          tokenomicsLines.style.transform = 'translate(' + ((scrollSpeed - 50) * 12) + '%, 0)';
          tokenomicsLines2.style.transform = 'translate(' + ((scrollSpeed - 50) * 14) + '%, 0)';
        }

        if (howToBuyTop <= 50) {
          howToBuyLines.style.transform = 'translate(-' + ((scrollSpeed - 50) * 12) + '%, 0)';
          howToBuyLines2.style.transform = 'translate(-' + ((scrollSpeed - 50) * 14) + '%, 0)';
        }

        if (partnersTop <= 50) {
          partnersShapes.style.transform = 'rotate(-' + ((scrollSpeed - 50) * 20) + 'deg)';
          partnersLines.style.transform = 'translate(' + ((scrollSpeed - 80) * 10) + '%, 0)';
        }
      });
    }

    scrollMain();
  }
})();
